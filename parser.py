import requests
from bs4 import BeautifulSoup

url = 'https://habr.com/ru/all/'
response = requests.get(url).text

data = BeautifulSoup(response, 'html.parser')

for article in data.find_all('h2', class_ = 'tm-title tm-title_h2'):
    title = article.a.span.text
    link = 'https://habr.com'+article.a['href']
    print(title, link)